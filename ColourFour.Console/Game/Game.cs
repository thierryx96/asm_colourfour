﻿using System;
using System.Linq;

namespace ColourFour
{
    public class Game
    {
        public enum TurnOutcomeType
        {
            Win,
            Draw
        }

        private readonly PlayerColorType _startingPlayer;

        private readonly Tray _tray;

        /// <summary>
        ///     Start a new game
        /// </summary>
        /// <param name="rowsCount"></param>
        /// <param name="columnsCount"></param>
        /// <param name="startingPlayer"></param>
        public Game(int columnsCount, int rowsCount, PlayerColorType startingPlayer = PlayerColorType.Yellow)
        {
            _startingPlayer = startingPlayer;
            _tray = new Tray(columnsCount, rowsCount);
        }

        /// <summary>
        ///     Current turn number, a game start a turn 1
        /// </summary>
        public int TurnNumber => _tray.PlacedTokensCount + 1;

        /// <summary>
        ///     Number of turn max, a games ends when the tray has been filled with token
        /// </summary>
        public int MaxTurns => _tray.RowsCount*_tray.ColumnsCount;


        /// <summary>
        ///     Get the player that needs to place a token
        /// </summary>
        public PlayerColorType CurrentPlayer => TurnNumber%2 == 1
            ? _startingPlayer
            : (_startingPlayer == PlayerColorType.Red ? PlayerColorType.Yellow : PlayerColorType.Red);

        /// <summary>
        ///     Place  a token for the current player
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <returns>If this currenty player won or end up in a draw</returns>
        public TurnOutcomeType? Play(int columnIndex)
        {
            return PlayTurn(CurrentPlayer, columnIndex);
        }

        private TurnOutcomeType? PlayTurn(PlayerColorType player, int columnIndex)
        {
            var position = _tray.PlaceToken(player, columnIndex);
            var wins = _tray.IsWinningSequence(player, position);

            // player is winner
            if (wins)
            {
                return TurnOutcomeType.Win;
            }
                // no more token left to play - no win
            if (TurnNumber == MaxTurns + 1)
            {
                return TurnOutcomeType.Draw;
            }
                // we continue
            return null;
        }

        /// <summary>
        ///     diplsay the current state of a game
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"Turn:{TurnNumber}/{MaxTurns} Player:{CurrentPlayer}" + Environment.NewLine
                   + _tray;
        }

        internal string[] ToLines()
        {
            return _tray.ToLines().ToArray();
        }
    }
}