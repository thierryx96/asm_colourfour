﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Common;

[assembly: InternalsVisibleTo("ColourFour.Tests")]
// a bit ugly, but keep the tray encapsulated whilst having it testable (white box testing ... )
namespace ColourFour
{
    public enum PlayerColorType
    {
        Red,
        Yellow
    }

    /// <summary>
    /// Represent a "colourfour" board
    /// </summary>
    internal class Tray
    {
        private readonly Dictionary<Coordinate, PlayerColorType> _tokens;

        public readonly int TokensCountForWinning = 4; // could be added as a ctor variable parameter ... 

        public Tray(int columnsCount, int rowsCount)
        {
            if (rowsCount < TokensCountForWinning)
                throw new ArgumentOutOfRangeException(nameof(rowsCount), rowsCount,
                    $"A tray must a number of rows greater or equal than {TokensCountForWinning}.");
            if (columnsCount < TokensCountForWinning)
                throw new ArgumentOutOfRangeException(nameof(columnsCount), rowsCount,
                    $"A tray must a number of columns greater or equal than {TokensCountForWinning}.");

            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            _tokens = new Dictionary<Coordinate, PlayerColorType>();
        }

        // only for testing purposes
        internal Tray(int rowsCount, int columnsCount, string s) : this(rowsCount, columnsCount)
        {
            _tokens = s.Replace(" ", string.Empty)
                .Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
                .Reverse()
                .SelectMany((row, y) => row.Trim().
                    Select(
                        (cell, x) =>
                            new KeyValuePair<Coordinate, PlayerColorType?>(new Coordinate(x + 1, y + 1),
                                CharToPlayerColor(cell))))
                .Where(cell => cell.Value.HasValue)
                .ToDictionary(v => v.Key, v => v.Value.Value);
        }

        public int RowsCount { get; }

        public int ColumnsCount { get; }

        public int PlacedTokensCount => _tokens.Count;

        /// <summary>
        /// Get the content of a cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private PlayerColorType? GetCoordinate(int x, int y)
        {
            var coordinate = new Coordinate(x, y);
            if (_tokens.ContainsKey(coordinate))
            {
                return _tokens[coordinate];
            }
            return null;
        }

        /// <summary>
        /// Count the number of tokens consecutively aligned 
        /// starting from a point (origin) and towards a specific direction 
        /// </summary>
        /// <param name="color">Red/Yellow</param>
        /// <param name="origin">Where we start to count from</param>
        /// <param name="transX">direction X</param>
        /// <param name="transY">direction Y</param>
        /// <param name="offset">how far we are from origin</param>
        /// <returns></returns>
        private int CountSequenceTokens(
            PlayerColorType color, // static
            Coordinate origin, // static
            int transX, // static
            int transY, // static
            int offset = 0) // recursive
        {
            var x = origin.X + offset*transX;
            var y = origin.Y + offset*transY;

            // n = 0, we peek the current token
            var currentToken = GetCoordinate(x, y);

            // token aligned found, we carry on
            if (currentToken.HasValue && currentToken.Value == color)
            {
                return 1 + CountSequenceTokens(color, origin, transX, transY, offset + 1);
            }

            // no more token found (exit condition)
            return 0;
        }

        /// <summary>
        /// Count the number of tokens consecutively aligned
        /// starting from a point (origin) in both directions 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="origin"></param>
        /// <param name="transX"></param>
        /// <param name="transY"></param>
        /// <returns></returns>
        private int GetFullSequence(
            PlayerColorType color,
            Coordinate origin,
            int transX,
            int transY)
        {
            return CountSequenceTokens(color, origin, transX*-1, transY*-1) +
                   CountSequenceTokens(color, origin, transX*1, transY*1);
        }

        public bool IsWinningSequence(
            PlayerColorType color, // static
            Coordinate origin)
        {
            // combinations of possible translations from origin for searching for consecutive tokens
            return
                new[]
                {
                    new Coordinate(0, 1), // horizontal, left to right
                    new Coordinate(1, 0), // vertical, downward
                    new Coordinate(1, 1), // diagonal, upward, left to right
                    new Coordinate(1, -1) // diagonal, downward, left to right
                }
                    // check for a consecutive line of tokens for the tranlations given, reduce by result 
                    // ( =  if one winning line of token found, we don't search anymore)
                    .Aggregate(false, (a, c) => a || GetFullSequence(color, origin, c.X, c.Y) > TokensCountForWinning);
        }

        /// <summary>
        /// Get the number of tokens in a column
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private int GetColumnTokensCount(int x)
        {
            return _tokens.Keys.Count(v => v.X == x);
        }

        /// <summary>
        ///     Add a token to any column of the tray
        /// </summary>
        /// <param name="player"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        public Coordinate PlaceToken(PlayerColorType player, int columnIndex)
        {
            // out of boudary of the tray
            if (columnIndex < 1 || columnIndex > ColumnsCount)
                throw new ArgumentOutOfRangeException(nameof(columnIndex), columnIndex,
                    $"A token can only be place on an existing column, column index must be included between 1 and {ColumnsCount}.");

            var currentTokensCount = GetColumnTokensCount(columnIndex);

            // column is full 
            if (currentTokensCount >= RowsCount)
                throw new InvalidOperationException(
                    $"Column number:{columnIndex} is already full. A token cannot be added.");
            var coordinate = new Coordinate(columnIndex, currentTokensCount + 1);
            _tokens.Add(coordinate, player);

            return coordinate;
        }

        private static PlayerColorType? CharToPlayerColor(char c)
        {
            switch (c)
            {
                case 'r':
                    return PlayerColorType.Red;
                case 'y':
                    return PlayerColorType.Yellow;
                case 'o':
                    return null;
                default:
                    throw new ArgumentException($"Invalid Cell Content: {c}");
            }
        }

        private static char PlayerColorToChar(PlayerColorType? content)
        {
            return content.HasValue ? (content.Value == PlayerColorType.Red ? 'r' : 'y') : 'o';
        }

        internal IEnumerable<string> ToLines()
        {
            var lines = new List<string>();
            for (var iRow = RowsCount; iRow > 0; iRow--)
            {
                var line = new StringBuilder();
                for (var iColumn = 1; iColumn <= ColumnsCount; iColumn++)
                {
                    line.Append(PlayerColorToChar(GetCoordinate(iColumn, iRow)));
                }
                lines.Add(line.ToString());
            }
            return lines;
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, ToLines());
        }
    }
}