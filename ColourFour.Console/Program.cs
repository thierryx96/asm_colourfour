﻿using System;
using System.Linq;
using ColourFour;

namespace Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Game game = null;
            while (game == null)
            {
                System.Console.WriteLine("Please enter the board dimensions (number of rows, number of columns)");
                try
                {
                    var input = System.Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                    game = new Game(int.Parse(input.First()), int.Parse(input.Last()), PlayerColorType.Yellow);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex);
                }
            }

            Game.TurnOutcomeType? gameOutcome = null;
            var lastPlayer = game.CurrentPlayer; // player starting

            while (!gameOutcome.HasValue)
            {
                System.Console.WriteLine(game);
                System.Console.Write("Place token in column:");
                //System.Console.WriteLine($"{game.CurrentPlayer} turns");

                var iColumns = int.Parse(System.Console.ReadLine());
                lastPlayer = game.CurrentPlayer;

                try
                {
                    gameOutcome = game.Play(iColumns);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex);
                }
            }

            if (gameOutcome == Game.TurnOutcomeType.Win)
            {
                System.Console.WriteLine($"{lastPlayer} WINS!");
            }
            else if (gameOutcome == Game.TurnOutcomeType.Draw)
            {
                System.Console.WriteLine("DRAW!");
            }

            System.Console.WriteLine(game);
            System.Console.ReadLine();
        }
    }
}