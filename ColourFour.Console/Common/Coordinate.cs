﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common    
{
    /// <summary>
    /// Define a coordinate on a plan (x,y)
    /// </summary>
    public struct Coordinate : IEquatable<Coordinate>, IComparable<Coordinate>
    {


        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;            
        }

        public int X { get; }
        public int Y { get; }

        public override int GetHashCode()
        {
            return X ^ Y;
        }

        public bool Equals(Coordinate other)
        {
            return X == other.X && Y == other.Y;
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }

        public static bool operator ==(Coordinate left, Coordinate right)
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator !=(Coordinate left, Coordinate right)
        {
            return !(left == right);
        }

        public int CompareTo(Coordinate other)
        {
            var compareX = this.X.CompareTo(other.X);
            return compareX == 0 ? this.Y.CompareTo(other.Y) : compareX;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Coordinate && Equals((Coordinate)obj);
        }

        /// <summary>
        /// generate a list of all coordinates values for a plan
        /// </summary>
        /// <param name="startX">origin X</param>
        /// <param name="startY">origin Y</param>
        /// <param name="countX">number of points X</param>
        /// <param name="countY">number of points Y</param>
        /// <returns></returns>
        public static IEnumerable<Coordinate> GenerateCoordinates(int startX, int startY, int countX, int countY)
            => Enumerable.Range(startX, countX)
                .SelectMany(x => Enumerable.Range(startY, countY).Select(y => new Coordinate(x, y)));


    }
}
