#This repository contains##

* Proposed solution to the assessment problem source code with C# with Visual Studio 2015.
* the executable can be found at the root of this repo, within /ColourFour.Console.zip 
* A wiki page with explanation on the design of the solution (https://bitbucket.org/thierryx96/exercise_colourfour/wiki/Home)

## Prerequisites ##

* MS .net Framework 4.6.1
* Git to get the code (optional)
* Visual Studio 2013 or 2015 to open and edit it (optional)

## Run the executable ###

The executable version of this program can found at the root of this repo /ColourFour.Console.zip 

```
ColourFour.Console
```

## Run in Debug Mode (Visual Studio 2013) ###

* Open the .sln file
* Run the main project (ColourFour.Console) as debug

## Output 
```
ColourFour.Console
```

```
Please enter the board dimensions (number of rows, number of columns)
5 6
Turn:1/30 Player:Yellow
ooooo
ooooo
ooooo
ooooo
ooooo
ooooo
Place token in column:1

Turn:2/30 Player:Red
ooooo
ooooo
ooooo
ooooo
ooooo
yoooo
Place token in column:5

Turn:3/30 Player:Yellow
ooooo
ooooo
ooooo
ooooo
ooooo
yooor

...
```