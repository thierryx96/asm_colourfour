﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColourFour.Tests
{

    /// <summary>
    /// Scenario Five (Invalid board dimensions) 
    /// Scenario Six(Invalid move)
    /// </summary>
    [TestClass]
    public class InvalidOperationsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Scenario5_InvalidBoardDimensions_WhenNumberOfRowsTooSmall()
        {
            new Game(1, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Scenario5_InvalidBoardDimensions_WhenNumberOfColumnsTooSmall()
        {
            new Game(5, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Scenario6_InvalidMove_WhenColumnIsFull()
        {
            var numberOfRows = 5;
            var game = new Game(7, numberOfRows);

            for (int i = 1; i <= numberOfRows+1; i++)
            {
                game.Play(2);
            }            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Scenario6_InvalidMove_WhenColumnIsOutOfBoundMin()
        {
            var game = new Game(6,5);
            game.Play(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Scenario6_InvalidMove_WhenColumnIsOutOfBoundMax()
        {
            var game = new Game(6, 5);
            game.Play(0);
        }
    }
}
