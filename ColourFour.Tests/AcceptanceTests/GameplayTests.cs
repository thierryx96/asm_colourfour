﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ColourFour;

namespace ColourFour.Tests
{
    /// <summary>
    ///Scenario One (Yellow wins - Horizontal) 
    ///  Scenario Two (Red wins - Vertical) 
    ///  Scenario Three (Yellow wins diagonal) 
    ///  Scenario Four (Draw) 
    /// </summary>
    [TestClass]
    public class GameplayTests
    {
        private Game _game;
        private int _columnsCount;
        private int _rowsCount;

        [TestInitialize]
        public void InitTests()
        {
            _columnsCount = 8;
            _rowsCount = 6;

            _game = new Game(_columnsCount, _rowsCount, PlayerColorType.Yellow);            
        }

        private static Game.TurnOutcomeType? ApplyNonWinningPattern4By4(Game game, int offset = 0)
        {
            Assert.IsNull(game.Play(1+offset)); // 1                
            Assert.IsNull(game.Play(3+offset)); // 2             
            Assert.IsNull(game.Play(1+offset)); // 1                
            Assert.IsNull(game.Play(3+offset)); // 2                    
            Assert.IsNull(game.Play(1+offset)); // 1                
            Assert.IsNull(game.Play(3+offset)); // 2    
                            
            Assert.IsNull(game.Play(2+offset)); // 1                
            Assert.IsNull(game.Play(4+offset)); // 2             
            Assert.IsNull(game.Play(2+offset)); // 1                
            Assert.IsNull(game.Play(4+offset)); // 2                    
            Assert.IsNull(game.Play(2+offset)); // 1                
            Assert.IsNull(game.Play(4+offset)); // 2    
                               
            Assert.IsNull(game.Play(3+offset)); // 1                
            Assert.IsNull(game.Play(1+offset)); // 2  
            Assert.IsNull(game.Play(4+offset)); // 1                
            var outcome = game.Play(2+offset);  // 2  
            return outcome; // outcome is always draw, for a 4x4 grid
        }

        

        [TestMethod]
        public void Scenario1_YellowWinsHorizontal_OnBottomRow()
        {            
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(6)); // Y //1st
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y //2nd
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(5)); // Y //3rd
            Assert.IsNull(_game.Play(7)); // R

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(3); // Y // 4th

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);            
        }

        [TestMethod]
        public void Scenario1_YellowWinsHorizontal_OnMiddleRow()
        {
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R
            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R

            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(5)); // R

            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(1)); // R

            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(1)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(7)); // R

            


            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(5); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario1_YellowWinsHorizontal_OnTopRow()
        {
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(7)); // R

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(6); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario1_YellowWinsHorizontal_OnTopRowStartingFromRight()
        {
            Assert.IsNull(_game.Play(1 + _columnsCount  -1)); // Y
            Assert.IsNull(_game.Play(1 + _columnsCount - 2)); // R
            Assert.IsNull(_game.Play(1 + _columnsCount - 3)); // Y
            Assert.IsNull(_game.Play(1 + _columnsCount - 2)); // R

            Assert.IsNull(_game.Play(1 + _columnsCount - 4)); // Y
            Assert.IsNull(_game.Play(1 + _columnsCount - 2)); // R
            Assert.IsNull(_game.Play(1 + _columnsCount - 5)); // Y
            Assert.IsNull(_game.Play(1 + _columnsCount - 7)); // R

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(1 + _columnsCount - 6); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario2_RedWinsVertical_OnLeftColumn()
        {
            var winningColumn = 1;

            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(winningColumn)); // R
            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R

            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(5)); // R

            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(winningColumn)); // R

            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(winningColumn)); // R

            Assert.IsNull(_game.Play(4)); // Y

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Red, _game.CurrentPlayer);
            var outcome = _game.Play(winningColumn); // R

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario2_RedWinsVertical_OnCenterColumn()
        {
            var winningColumn = 4;
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R
            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R

            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(5)); // R

            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(1)); // R

            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(winningColumn)); // R

            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(winningColumn)); // R
            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(winningColumn)); // R
            Assert.IsNull(_game.Play(6)); // Y

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Red, _game.CurrentPlayer);
            var outcome = _game.Play(winningColumn); // R

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario2_RedWinsVertical_OnRightColumn()
        {
            var winningColumn = _columnsCount;
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R
            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R

            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(5)); // R

            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(1)); // R

            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(winningColumn)); // R

            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(winningColumn)); // R
            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(winningColumn)); // R
            Assert.IsNull(_game.Play(6)); // Y

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Red, _game.CurrentPlayer);
            var outcome = _game.Play(winningColumn); // R

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario3_YellowWinsDiagonalUpdward_FromOrigin()
        {
            // 1st
            Assert.IsNull(_game.Play(1)); // Y

            // 2nd
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(2)); // Y

            // 3rd
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(3)); // Y

            // 4th
            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(6)); // R

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(4); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario3_YellowWinsDiagonalUpdward_FromCenter()
        {
            // Filling
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(6)); // R
            Assert.IsNull(_game.Play(7)); // Y
            Assert.IsNull(_game.Play(8)); // R

            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(6)); // R
            Assert.IsNull(_game.Play(7)); // Y
            Assert.IsNull(_game.Play(8)); // R

            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(5)); // Y // 1st
            Assert.IsNull(_game.Play(6)); // R
            Assert.IsNull(_game.Play(7)); // Y
            Assert.IsNull(_game.Play(8)); // R

            // 2nd
            Assert.IsNull(_game.Play(6)); // Y // 2nd

            // 3rd
            Assert.IsNull(_game.Play(7)); // R
            Assert.IsNull(_game.Play(7)); // Y 3rd

            // 4th
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(8)); // Y 
            Assert.IsNull(_game.Play(8)); // R

            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(8); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario2_YellowWinsDiagonalDownward_FromLeftUp()
        {
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R
            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(2)); // R

            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R
            Assert.IsNull(_game.Play(4)); // Y
            Assert.IsNull(_game.Play(3)); // R

            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(5)); // R

            Assert.IsNull(_game.Play(2)); // Y
            Assert.IsNull(_game.Play(1)); // R

            Assert.IsNull(_game.Play(3)); // Y
            Assert.IsNull(_game.Play(4)); // R

            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(4)); // R
            Assert.IsNull(_game.Play(3)); // Y

            Assert.IsNull(_game.Play(4)); // R


            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Yellow, _game.CurrentPlayer);
            var outcome = _game.Play(2); // Y

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }

        [TestMethod]
        public void Scenario3_RedWinsDiagonalUpdward_FromLeftToBottom()
        {
            // 1st
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R
            Assert.IsNull(_game.Play(1)); // Y
            Assert.IsNull(_game.Play(1)); // R // 1st
            Assert.IsNull(_game.Play(2)); // Y

            // 2nd
            Assert.IsNull(_game.Play(2)); // R
            Assert.IsNull(_game.Play(3)); // Y 
            Assert.IsNull(_game.Play(2)); // R // 2nd


            // 3rd
            Assert.IsNull(_game.Play(5)); // Y
            Assert.IsNull(_game.Play(3)); // R // 3rd

            Assert.IsNull(_game.Play(5)); // Y


            // Current player (the one that will place the winning token)
            Assert.AreEqual(PlayerColorType.Red, _game.CurrentPlayer);
            var outcome = _game.Play(4); // R // 4th

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Win);
        }


        [TestMethod]
        public void Scenario4_Draw_4By4()
        {
            var game = new Game(4,4);

            var outcome = ApplyNonWinningPattern4By4(game);

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Draw);
        }

        [TestMethod]
        public void Scenario4_Draw_8By8()
        {
            var game = new Game(8, 8);
            Assert.IsNull(ApplyNonWinningPattern4By4(game, 4));
            Assert.IsNull(ApplyNonWinningPattern4By4(game));
            Assert.IsNull(ApplyNonWinningPattern4By4(game, 4));
            var outcome = ApplyNonWinningPattern4By4(game);

            Assert.IsTrue(outcome.HasValue && outcome.Value == Game.TurnOutcomeType.Draw);
        }

        
    }
}
