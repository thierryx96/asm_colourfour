﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ColourFour;
using Common;

namespace ColourFour.Tests
{
    [TestClass]
    public class TrayTests
    {

        [TestMethod]
        public void Tray_CheckIsWinning_WithNoWinNoDraw()
        {
            var tray = new Tray(10, 8, @"
                yooooyoy
                yooooyoy
                oooooooy
                yooooyoo
                ooyoyoyo
                oooooooo
                yooooyoy
                yooooyoy
                yooooyoy
                oooooooo");
            
            foreach (var coord in Coordinate.GenerateCoordinates(1, 1, tray.ColumnsCount, tray.RowsCount))
            {
                Assert.IsFalse(tray.IsWinningSequence(PlayerColorType.Red, coord));
                Assert.IsFalse(tray.IsWinningSequence(PlayerColorType.Yellow, coord));
            }
        }

        [TestMethod]
        public void Tray_CheckIsWinning_WithYellowWins()
        {
            var tray = new Tray(10, 8, @"
                yooooyoy
                yooooyoy
                yooooooy
                yooooyoo
                ooyoyoyo
                oooooooo
                yooooyoy
                yooooyoy
                yooooyoy
                oooooooo");

            

            foreach (var coord in Coordinate.GenerateCoordinates(1, 7, 1, 4))
            {
                var wins = tray.IsWinningSequence(PlayerColorType.Yellow, coord);
                Assert.IsTrue(wins);
            }
        }

    }
}
